#!/usr/bin/sh

echo "===== PEP 8 ====="
pep8 --ignore=W191 *.py
echo
echo "===== PYLINT ====="
pylint *.py
