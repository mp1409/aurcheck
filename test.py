import unittest

import aurcheck


class Test(unittest.TestCase):
	def test_version_comparison(self):
		version_pairs = [
			('2.4.20-1', '2.4.20-1', 'OK'),
			('2.4.20-1', '2.4.20-2', 'UPDATE'),
			('0.4.3_3-1', '0.4.3_3-1', 'OK'),
			('0.4.3_3-1', '0.4.3_4-1', 'UPDATE'),
			('1:1.4.8.885-1', '1:1.4.8.885-1', 'OK'),
			('1:1.4.8.885-1', '2:1.4.8.885-1', 'UPDATE'),
			('293.2e35fea-1', '293.2e35fea-1', 'OK'),
			('293.2e35fea-1', '294.2e35fea-1', 'UPDATE'),
			('r92.612c5a1-1', 'r92.612c5a1-1', 'OK'),
			('r92.612c5a1-1', 'r93.612c5a1-1', 'UPDATE'),
			('1.9.9-1', '1.9.9-1', 'OK'),
			('1.9.9-1', '1.9.10-1', 'UPDATE'),
			('1.9.9', '1.9.9-1', 'UPDATE')
		]

		for local, remote, status in version_pairs:
			self.assertEqual(aurcheck.getstatus(local, remote), status)


unittest.main()
