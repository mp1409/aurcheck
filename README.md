# aurcheck
aurcheck is a short Python script which checks if packages on an Arch Linux machine installed from the [Arch User Repository (AUR)](https://aur.archlinux.org/) need updating.

## Installation
aurcheck requires only the Python interpreter and modules from the standard library, both are located in the [*python* package](https://www.archlinux.org/packages/extra/x86_64/python/).
Once *python* is installed, aurcheck can be run by simply executing the *aurcheck.py* file.

aurcheck could be installed for example by:
```
sudo cp aurcheck.py /usr/local/bin/aurcheck
sudo chmod 755 /usr/local/bin/aurcheck
```

And then run by simply typing:
```
aurcheck
```

## Usage
A list of updateable packages is printed by:
```
aurcheck [check]
```

All list of all packages installed from AUR is shown by:
```
aurcheck all
```

Snapshots of the updateable packages can be downloaded and extracted by:
```
aurcheck get
```

## Internals
aurcheck uses the [AUR API](https://aur.archlinux.org/rpc.php) to retrieve packet versions and snapshot URLs.
