#!/usr/bin/python

import argparse
import itertools
import json
from os import remove
from os.path import basename
import re
from shutil import unpack_archive
from subprocess import check_output
from threading import Thread
from urllib.request import urlopen

VERSION = '0.2'


def getstatus(localversion, remoteversion):
	"""Returns the status of a package by comparing its version numbers."""
	splitter = re.compile(r'[^A-Za-z0-9]')
	local = splitter.split(localversion)
	remote = splitter.split(remoteversion)

	for l, r in itertools.zip_longest(local, remote, fillvalue='0'):
		if l.isdigit() and r.isdigit():
			l, r = int(l), int(r)

		if l < r:
			return 'UPDATE'
		elif l == r:
			pass
		else:
			return 'UNKNOWN'
	return 'OK'


def addremoteinfo(p):
	"""Adds the `Remote` and `Status` fields to package `p`."""
	aururl = 'https://aur.archlinux.org'
	requesturl = aururl + '/rpc.php?type=info&arg=' + p['Name']
	with urlopen(requesturl) as response:
		data = json.loads(response.read().decode())
		if data['resultcount'] == 1:
			p['Remote'] = data['results']['Version']
			p['Status'] = getstatus(p['Local'], p['Remote'])
			p['SnapshotURL'] = aururl + data['results']['URLPath']
		else:
			p['Remote'] = ' '
			p['Status'] = 'UNKNOWN'


def getpackagelist():
	"""Returns a list of manually installed packages by parsing pacman
	output."""
	P = []
	T = []

	for line in check_output(['pacman', '-Qm']).decode().split('\n'):
		if len(line) == 0:
			continue

		p = {}
		p['Name'], p['Local'] = line.split()

		t = Thread(target=addremoteinfo, args=(p,))
		t.start()
		T.append(t)

		P.append(p)

	for t in T:
		t.join()

	return P


def printupdateinfo(P):
	"""Prints a line with information whether package list `P` contains
	updateable packages."""
	updateable = len([p for p in P if p['Status'] == 'UPDATE'])
	unknown = len([p for p in P if p['Status'] == 'UNKNOWN'])

	if updateable == 0:
		print('No packages waiting for an update.')
	elif updateable == 1:
		print('1 package waiting for an update.')
	else:
		print('{} packages waiting for an update.'.format(updateable))
	if unknown == 1:
		print('1 unknown package!')
	elif unknown > 1:
		print('{} unknown packages!'.format(unknown))


def printtable(L, columns):
	"""Prints list `L` in tabular from, using keys given in the list
	`columns`."""
	if L == []:
		return

	formats = []
	for c in columns:
		width = max(len(l[c]) for l in L)
		formats.append('{' + c + ':' + str(width) + '}')
	lineformat = '\t'.join(formats)

	header = lineformat.format(**{c: c for c in columns})
	print(header)

	for l in L:
		line = lineformat.format(**l)
		print(line)


def retrievesnapshot(p):
	"""Downloads the snapshot of package `p`, extracts it and removes the
	archive file."""
	filename = basename(p["SnapshotURL"])

	with urlopen(p['SnapshotURL']) as response:
		with open(filename, 'wb') as f:
			f.write(response.read())

	unpack_archive(filename)
	remove(filename)


def gathersnapshots(P):
	"""Downloads the snapshots for all updateable packages in `P`."""
	T = []
	for p in P:
		if p['Status'] == 'UPDATE':
			t = Thread(target=retrievesnapshot, args=(p,))
			t.start()
			T.append(t)
	for t in T:
		t.join()
	print('Downloaded all snapshots.')


if __name__ == "__main__":
	argparser = argparse.ArgumentParser(
		description='Check for updates of packages installed from the Arch User '
		'Repository (AUR).'
	)
	argparser.add_argument(
		'command', nargs='?', choices=('check', 'all', 'get'), default='check',
		help='"check" lists all updateable packages, "all" lists all '
		'packages, "get" downloads and extracts snapshots'
	)
	argparser.add_argument(
		'-v', '--version', action='store_true', help='show version'
	)
	args = argparser.parse_args()

	if args.version is True:
		print(VERSION)
	elif args.command == 'check':
		P = getpackagelist()
		printupdateinfo(P)
		L = [p for p in P if p['Status'] != 'OK']
		printtable(L, ['Name', 'Local', 'Remote', 'Status'])
	elif args.command == 'all':
		P = getpackagelist()
		printupdateinfo(P)
		printtable(P, ['Name', 'Local', 'Remote', 'Status'])
	elif args.command == 'get':
		P = getpackagelist()
		printupdateinfo(P)
		gathersnapshots(P)
	else:
		raise RuntimeError('Error while parsing arguments')
